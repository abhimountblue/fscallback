const fs = require('fs')



function deleteFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, cb) {
  let count = 0
  for (let file = 1; file <= randomNumberOfFiles; file++) {
    let filePath = absolutePathOfRandomDirectory + '/' + file + '.json'
    fs.unlink(filePath, (err) => {
      if (err) {
        console.error(err)
      }
      else {
        count++
        if (randomNumberOfFiles == count) {
          console.log(`all file is deleted`)
          cb(null)
        }
      }
    })
  }
}

function creatfiles(absolutePathOfRandomDirectory, randomNumberOfFiles, cb) {
  let count = 0;
  for (let file = 1; file <= randomNumberOfFiles; file++) {
    let filePath = absolutePathOfRandomDirectory + '/' + file + '.json'
    fs.writeFile(filePath, '', (err) => {
      if (err) {
        console.error(err)
      }
      else {
        count++
        if (count == randomNumberOfFiles) {
          console.log(`all file is created`)
          cb(null)
        }
      }
    })
  }
}


function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
  fs.mkdir(absolutePathOfRandomDirectory, (err) => {
    if (err) {
      console.error(err)
    } else {
      console.log(`directory is created`)
      creatfiles(absolutePathOfRandomDirectory, randomNumberOfFiles, (err) => {
        if (err) {
          console.error(err)
        }
        else {
          console.log(`Number of random files created`)
          deleteFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, (err) => {
            if (err) {
              console.error(err)
            } else {
              fs.rmdir(absolutePathOfRandomDirectory, (err) => {
                if (err) {
                  console.error(err)
                } else {
                  console.log("Directory is deleted.")
                }
              })
            }
          })
        }

      })
    }
  })
}

module.exports = fsProblem1