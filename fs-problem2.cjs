const fs = require("fs")

function fsProblem2(file) {
    // reading file using fs.readFile method
  fs.readFile(file, "utf-8", (err, lipsumFileData) => {
    if (err) {
      console.log(err)
    } else {
        // lipsum data will transform into uppercase create a file name upperCase.json
      fs.writeFile(
        "./upperCase.json",
        JSON.stringify(lipsumFileData.toUpperCase(), null, 2),
        (err) => {
          if (err) {
            console.log(err)
          } else {
            // fs.appendFile method will add upperCase.json text in filenames.txt
            fs.appendFile("./filenames.txt", "upperCase.json", (err) => {
              if (err) {
                console.log(err)
              } else {
                fs.readFile("./upperCase.json", "utf-8", (err, upperCaseFileData) => {
                  if (err) {
                    console.log(err)
                  } else {
                      const sentencesArray = upperCaseFileData.toLowerCase().split("\n")
                      // create a file name sentence.json and put sentencesArray in sentence.json
                    fs.writeFile("./sentence.json",JSON.stringify(sentencesArray, null, 2),(err) => {
                        if (err) {
                          console.log(err)
                        } else {
                            // fs.appendFile method will add sentence.json text in filenames.txt
                          fs.appendFile("./filenames.txt"," sentence.json",(err) => {
                              if (err) {
                                console.log(err)
                              } else {
                                fs.readFile("./sentence.json","utf-8",(err) => {
                                    if (err) {
                                      console.log(err)
                                    } else {
                                      const sortedContent = sentencesArray.sort()
                                        // create a file name sortedContent.json and put sortedContent in sortedContent.json
                                      fs.writeFile("./sortContent.json",JSON.stringify(sortedContent, null, 2),(err) => {
                                          if (err) {
                                            console.log(err)
                                          } else {
                                            // fs.appendFile method will add sortedContent.json text in filenames.txt
                                            fs.appendFile("./filenames.txt"," sortContent.json",(err) => {
                                                if (err) {
                                                  console.log(err)
                                                } else {
                                                  fs.readFile("./filenames.txt","utf-8",(err, filesNameString) => {
                                                      if (err) {
                                                        console.log(err)
                                                      } else {
                                                        const filesNameArray =filesNameString.split(" ")
                                                        // created seperate funtion to delete files
                                                        deleteFile(filesNameArray)
                                                      }
                                                    }
                                                  )
                                                }
                                              }
                                            )
                                          }
                                        }
                                      )
                                    }
                                  }
                                )
                              }
                            }
                          )
                        }
                      }
                    )
                  }
                })
              }
            })
          }
        }
      )
    }
  })
}


function deleteFile(arrayOfFiles, number = 0) {
  if (arrayOfFiles.length > number) {
    // deleting a file using fs.unlink method
    fs.unlink(`${arrayOfFiles[number]}`, (err) => {
      if (err) {
        console.error(err)
      } else {
        console.log(`${arrayOfFiles[number]} ` + "file is deleted")
        // calling a function till length of arrayOfFiles recursively
        deleteFile(arrayOfFiles, number + 1)
      }
    })
  }
  return
}

module.exports = fsProblem2
